package web.service.role;

import web.model.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAllRoles();
}
