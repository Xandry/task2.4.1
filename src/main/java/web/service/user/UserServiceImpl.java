package web.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import web.dao.role.RoleDao;
import web.dao.user.UserDao;
import web.model.Role;
import web.model.User;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    private final RoleDao roleDao;

    @Autowired
    public UserServiceImpl(UserDao userDao, RoleDao roleDao) {
        this.userDao = userDao;
        this.roleDao = roleDao;
    }


    @Override
    public User getUser(long id) {
        return userDao.getUser(id);
    }

    @Override
    public User getByEmail(String email) {
        return userDao.getUserByEmail(email);
    }

    @Override
    public List<User> getAllUsers() {
        return userDao.getAllUsers();
    }

    @Transactional
    @Override
    public void deleteUser(long id) {
        User user = userDao.getUser(id);
        for (Role role : user.getRoles()) {
            role.getUsers().remove(user);
        }
        userDao.deleteUser(id);
    }

    @Transactional
    @Override
    public void deleteAll() {
        userDao.deleteAll();
    }

    @Transactional
    @Override
    public void addUser(User user) {
        userDao.addUser(user);
        for (Role role : user.getRoles()) {
            role.addUser(user);
            roleDao.update(role);
        }
    }

    @Transactional
    @Override
    public void updateUser(User user) {

        for (Role role : roleDao.getAllRoles()) {
            if (role.getUsers().contains(user) && !user.getRoles().contains(role)) {
                // user lost this role
                role.getUsers().remove(user);
                roleDao.update(role);
            } else if (!role.getUsers().contains(user) && user.getRoles().contains(role)) {
                // user gained this role
                role.getUsers().add(user);
                roleDao.update(role);
            }
        }
        userDao.updateUser(user);

    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userDao.getUserByEmail(s);
    }
}
