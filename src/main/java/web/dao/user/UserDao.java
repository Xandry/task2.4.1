package web.dao.user;


import web.model.User;

import java.util.List;

public interface UserDao {

    User getUser(long id);

    User getUserByEmail(String email);

    List<User> getAllUsers();

    void deleteUser(long id);

    void deleteAll();

    void addUser(User user);

    void updateUser(User user);

}
