package web.dao.role;

import web.model.Role;

import java.util.List;

public interface RoleDao {
    List<Role> getAllRoles();

    void update(Role role);
}
