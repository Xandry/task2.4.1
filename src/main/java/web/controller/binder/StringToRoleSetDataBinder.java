package web.controller.binder;

import org.springframework.core.convert.converter.Converter;
import web.model.Role;
import web.service.role.RoleService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StringToRoleSetDataBinder implements Converter<String, Set<Role>> {
    private final RoleService roleService;

    public StringToRoleSetDataBinder(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public Set<Role> convert(String roleName) {
        List<Role> allRoles = roleService.getAllRoles();
        HashSet<Role> resultSet = new HashSet<>();
        for (Role role : allRoles) {
            if (roleName.equalsIgnoreCase(role.getName())) {
                resultSet.add(role);
            }
        }
        return resultSet;
    }
}
