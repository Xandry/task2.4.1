package web.controller.binder;

import org.springframework.core.convert.converter.Converter;
import web.model.Role;
import web.service.role.RoleService;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StringArrayToRoleSetDataBinder implements Converter<String[], Set<Role>> {

    private final RoleService roleService;

    public StringArrayToRoleSetDataBinder(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public Set<Role> convert(String[] roles) {
        List<Role> allRoles = roleService.getAllRoles();
        HashSet<Role> resultSet = new HashSet<>();

        for (Role role : allRoles) {
            for (String roleName : roles) {
                if (roleName.equalsIgnoreCase(role.getName())) {
                    resultSet.add(role);
                }
            }
        }
        return resultSet;
    }
}
