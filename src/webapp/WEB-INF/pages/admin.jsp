<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Admin page</title>
</head>
<body>
<div>
    <h1>
        Admin page
    </h1>
    <a href="<c:url value="/logout"/>">Log out</a>
</div>
<table>
    <thead>
    <tr>
        <th>
            Role
        </th>
        <th>
            Email
        </th>
        <th>
            Password
        </th>
    </tr>
    </thead>
    <c:forEach var="user" items="${users}">
        <tr>
            <td>
                <c:forEach var="r" items="${user.roles}">
                    ${r.name}
                    <br>
                </c:forEach>
            </td>
            <td>
                    ${user.email}
            </td>
            <td>
                    ${user.password}
            </td>
            <td>
                <form:form action="/admin/delete/${user.id}" method="post">
                    <button type="submit">Delete</button>
                </form:form>
            </td>
            <td>
                <form:form action="/admin/update/${user.id}" method="get">
                    <button type="submit">Update</button>
                </form:form>

            </td>
        </tr>
    </c:forEach>
</table>
<div>
    <h2>
        User Creation
    </h2>
    <form:form action="/admin/create" method="post" modelAttribute="user">
        <label>
            Email:
            <form:input path="email"/>
        </label>
        <label>
            Password:
            <form:input path="password"/>
        </label>
        <form:checkboxes path="roles" items="${roles}"/>

        <button type="submit">Create</button>
    </form:form>
</div>
</body>
</html>
