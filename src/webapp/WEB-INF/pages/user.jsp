<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User page</title>
</head>
<body>
<div>
    <h1>
        User page
    </h1>
    <a href="<c:url value="/logout"/>">Log out</a>
</div>
<table>
    <thead>
    <tr>
        <th>
            Role
        </th>
        <th>
            Email
        </th>
        <th>
            Password
        </th>
    </tr>
    </thead>
    <tr>
        <td>
            <c:forEach var="r" items="${user.roles}">
                ${r.name}
                <br>
            </c:forEach>
        </td>
        <td>
            ${user.email}
        </td>
        <td>
            ${user.password}
        </td>
    </tr>
</table>
</body>
</html>
