<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Log in</title>
</head>
<body>

<form:form method="post" action="/login">
    <input name="j_username"/>
    <input type="password" name="j_password"/>
    <button type="submit">Log in</button>
</form:form>


</body>
</html>
